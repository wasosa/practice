package main

import "fmt"
import "datastructures/linkedlist"

func main() {
	fmt.Println(linkedlist.FromSlice([]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}))
}
