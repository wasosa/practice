package linkedlist

import "testing"

func assertEqual(t *testing.T, expected string, actual string) {
	if expected != actual {
		t.Errorf("\nExpected:\n%s\nActual:\n%s\n", expected, actual)
	}
}

func assertSlicesEqual(t *testing.T, expected []int, actual []int) {
	if len(expected) != len(actual) {
		t.Error("\nExpected:\n", expected, "\nActual:\n", actual)
		return
	}
	for i, v := range expected {
		if v != actual[i] {
			t.Error("\nExpected:\n", expected, "\nActual:\n", actual)
		}
	}
}

func TestString(t *testing.T) {
	var empty *ListNode
	assertEqual(t, "[]: ", empty.String())

	node := ListNode{1, nil}
	assertEqual(t, "[]: 1", node.String())

	node.next = &ListNode{2, nil}
	assertEqual(t, "[]: 1, 2", node.String())
}

func TestSlice(t *testing.T) {
	tds := [][]int{
		[]int{},
		[]int{0},
		[]int{1,2,3},
	}
	for _, td := range tds {
		assertSlicesEqual(t, td, FromSlice(td).Slice())
	}
}

func TestFromSlice(t *testing.T) {
	list := FromSlice([]int{1, 2, 3})
	expected := "[]: 1, 2, 3"
	actual := list.String()
	assertEqual(t, expected, actual)
}

func TestRemoveFirst(t *testing.T) {
	tds := []struct {
		Input []int
		Arg int
		Expected string
	} {
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 1,
			Expected: "[]: 2, 3, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 2,
			Expected: "[]: 1, 3, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 3,
			Expected: "[]: 1, 2, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 4,
			Expected: "[]: 1, 2, 3, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 5,
			Expected: "[]: 1, 2, 3, 4",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 6,
			Expected: "[]: 1, 2, 3, 4, 5",
		},
	}
	for _, td := range tds {
		list := FromSlice(td.Input)
		list = RemoveFirst(list, td.Arg)
		assertEqual(t, td.Expected, list.String())
	}
}

func TestRemoveLast(t *testing.T) {
	tds := []struct {
		Input []int
		Arg int
		Expected string
	} {
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 1,
			Expected: "[]: 2, 3, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 2,
			Expected: "[]: 1, 3, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 3,
			Expected: "[]: 1, 2, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 4,
			Expected: "[]: 1, 2, 3, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 5,
			Expected: "[]: 1, 2, 3, 4",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 6,
			Expected: "[]: 1, 2, 3, 4, 5",
		},
		{
			Input: []int{1, 1, 1 },
			Arg: 1,
			Expected: "[]: 1, 1",
		},
		{
			Input: []int{1, 2, 1 },
			Arg: 1,
			Expected: "[]: 1, 2",
		},
		{
			Input: []int{1, 1, 2 },
			Arg: 2,
			Expected: "[]: 1, 1",
		},
		{
			Input: []int{2, 1, 1 },
			Arg: 1,
			Expected: "[]: 2, 1",
		},
	}
	for _, td := range tds {
		list := FromSlice(td.Input)
		list = RemoveLast(list, td.Arg)
		assertEqual(t, td.Expected, list.String())
	}
}

func TestRemoveAll(t *testing.T) {
	tds := []struct {
		Input []int
		Arg int
		Expected string
	} {
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 1,
			Expected: "[]: 2, 3, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 2,
			Expected: "[]: 1, 3, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 3,
			Expected: "[]: 1, 2, 4, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 4,
			Expected: "[]: 1, 2, 3, 5",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 5,
			Expected: "[]: 1, 2, 3, 4",
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Arg: 6,
			Expected: "[]: 1, 2, 3, 4, 5",
		},
		{
			Input: []int{1, 1, 1 },
			Arg: 1,
			Expected: "[]: ",
		},
		{
			Input: []int{1, 2, 1 },
			Arg: 1,
			Expected: "[]: 2",
		},
		{
			Input: []int{1, 1, 2 },
			Arg: 1,
			Expected: "[]: 2",
		},
		{
			Input: []int{2, 1, 1 },
			Arg: 1,
			Expected: "[]: 2",
		},
	}
	for _, td := range tds {
		list := FromSlice(td.Input)
		list = RemoveAll(list, td.Arg)
		assertEqual(t, td.Expected, list.String())
	}
}

func TestReverse(t *testing.T) {
	tds := []struct {
		Input []int
		Expected []int
	} {
		{
			Input: []int{},
			Expected: []int{},
		},
		{
			Input: []int{1},
			Expected: []int{1},
		},
		{
			Input: []int{1, 2},
			Expected: []int{2, 1},
		},
		{
			Input: []int{1, 2, 3, 4, 5 },
			Expected: []int{5, 4, 3, 2, 1 },
		},
	}
	for _, td := range tds {
		list := FromSlice(td.Input)
		list = Reverse(list)
		assertSlicesEqual(t, td.Expected, list.Slice())
	}
}
