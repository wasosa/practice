package linkedlist

import "fmt"

type ListNode struct {
	value int
	next *ListNode
}

func (node *ListNode) String() string {
	output := ""
	output += fmt.Sprint("[]: ")
	for node != nil {
		output += fmt.Sprint(node.value)
		if node.next != nil {
			output += fmt.Sprint(", ")
		}
		node = node.next
	}
	return output
}

func (node *ListNode) Slice() []int {
	output := make([]int, 0)
	for node != nil {
		output = append(output, node.value)
		node = node.next
	}
	return output
}

func FromSlice(s []int) *ListNode {
	var node *ListNode
	var head *ListNode
	for _, n := range s {
		if node == nil {
			node = &ListNode{n, nil}
			head = node
		} else {
			node.next = &ListNode{n, nil}
			node = node.next
		}
	}
	return head
}

func RemoveFirst(head *ListNode, value int) *ListNode{
	anchor := &ListNode{0, head}
	node := anchor
	for node != nil && node.next != nil {
		if node.next.value == value {
			node.next = node.next.next
			return anchor.next
		}
		node = node.next
	}
	return anchor.next
}

func RemoveLast(head *ListNode, value int) *ListNode{
	anchor := &ListNode{0, head}
	node := anchor
	var last  *ListNode
	for node != nil && node.next != nil {
		if node.next.value == value {
			last = node
		}
		node = node.next
	}
	if last != nil {
		last.next = last.next.next
	}
	return anchor.next
}

func RemoveAll(head *ListNode, value int) *ListNode {
	anchor := &ListNode{0, head}
	node := anchor
	for node != nil && node.next != nil {
		for node.next != nil && node.next.value == value {
			node.next = node.next.next
		}
		node = node.next
	}
	return anchor.next
}

func Reverse(head *ListNode) *ListNode {
	if head == nil || head.next == nil {
		return head
	}
	tail := head.next
	head.next = nil
	temp := head
	head = Reverse(tail)
	tail.next = temp
	return head
}
